import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../entity/product';

@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.css']
})
export class TabComponent implements OnInit {

  @Input() list:Product[];

  constructor() { }

  ngOnInit() {
  }


}
