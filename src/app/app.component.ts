import { Component } from '@angular/core';
import { Link } from './menu/link';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-exercices';
  mainMenu: Link[] = [
    {
      label: 'About',
      url: '/about'
    },
    {
     label: 'List',
     url: '/list'
   }
  ];
}
