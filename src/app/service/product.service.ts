import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from '../entity/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private url = 'http://localhost:3000/products'
  constructor(private http:HttpClient) { }

  findAll():Observable<Product[]>{
    return this.http.get<Product[]>(this.url);
  }

  delete(id:number):Observable<Product>{
    return this.http.delete<Product>(this.url + id);

  }

  add(product:Product):Observable<Product>{
    return this.http.post<Product>(this.url, product);
  }

  update(product:Product):Observable<Product>{
    return this.http.put<Product>(this.url, product);
  }
}
