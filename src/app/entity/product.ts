export interface Product {
  id:number;
  label:string;
  price:number;
  category:string;
}
