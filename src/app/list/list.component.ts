import { Component, OnInit } from '@angular/core';
import { ProductService } from '../service/product.service';
import { Product } from '../entity/product';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
products  :Product[] = [];
  constructor(private service:ProductService) { }

  ngOnInit() {
    this.findAll();
  }

  findAll(){
    this.service.findAll().subscribe(response=> this.products = response);
  }



}
